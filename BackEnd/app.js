const express = require('express');
const customersRouter = require('./src/customer.routes');
const cors = require('cors');
const app = express();
const port = 3000;

app.use(cors());
app.use('/', customersRouter);
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  next();
})

app.listen(port, () => {
  console.log(`Server running at port: ${port}/`);
});
