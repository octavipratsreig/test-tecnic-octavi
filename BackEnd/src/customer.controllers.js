const CustomerService = require('./customer.services')    

exports.getCustomers = async function (req, res, next) {
    // Validate request parameters, queries using express-validator
    
    const page = req.params.page ? req.params.page : 1;
    const limit = req.params.limit ? req.params.limit : 10;
    try {
        const customers = await CustomerService.getCustomers({}, page, limit);
        return res.status(200).json({ status: 200, data: customers, message: "Succesfully Customers Retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.getCustomer = async function (req, res) {
    // Validate request parameters, queries using express-validator
    
    try {
        const customerId = req.params.id;
        const customer = await CustomerService.getCustomer({id: customerId});
        return res.status(200).json({ status: 200, data: customer, message: "Succesfully Customers Retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}