const express = require('express');
const router = express.Router();

const CustomerController = require('./customer.controllers');

router.get('/', CustomerController.getCustomers );
router.get('/:id', CustomerController.getCustomer );
 
module.exports = router;