//const Customer = require('./customer.model')
const fs = require('fs');

exports.getCustomers = async function (query, page, limit) {

    try {
        const rawdata = fs.readFileSync('./BD/customers.json');
        const data = JSON.parse(rawdata);
        return data.customers;
    } catch (e) {
        // Log Errors
        throw Error('Error while Paginating Customers')
    }
}

exports.getCustomer = async function (query) {

    try {
        const rawdata = fs.readFileSync('./BD/customers.json');
        const data = JSON.parse(rawdata);
        const customer = data.customers.find( c => c._id == query.id);
        return customer;
    } catch (e) {
        // Log Errors
        console.info(e);
        throw Error(`Error while getting Customer: ${query.id}`)
    }
}