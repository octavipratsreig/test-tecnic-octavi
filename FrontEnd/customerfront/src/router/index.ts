import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/customer/:id',
    name: 'Customer',
    component: () => import('../views/Customer.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
