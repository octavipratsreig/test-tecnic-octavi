import axios from "axios";

export default class CustomerDataService {
  static getSingleCustomer(id: any) {
    return axios.get(`http://localhost:3000/${id}`, {
      headers: {},
    });
  }
  static getAll(): any {
    return axios.get(`http://localhost:3000/`, {
      headers: {},
    });
  }
}
