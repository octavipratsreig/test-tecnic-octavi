# README #

## BackEnd

> NODE.js + Express

He fet servir una arquitectura per Capes: 

````
User -> Router <> Controller <> Service <> Model
````

Com a Base de Dades he utilitzat un JSON per fer-ho més senzill i dedicar més temps al FrontEnd.

### Setup
````
cd BackEnd
(potser cal instal·lar o actualitzar NODE.js)
npm i
node app.js

--- http://localhost:3000

    GET allCustomers
        /
    GET singleCustomer
        /:id
````


## FrontEnd

> Vue + Axios

> Typescript, SASS

He organitzat el projecte de la següent manera:

````
/views
/components
/services
/router
/@Theme
````

### Setup

````
cd FrontEnd/customerfront
(potser cal instal·lar o actualitzar NODE.js)
npm i
npm run serve

--- http://localhost:8080

    Home: /
    Customer: /customer/:id

````
Com és un projecte petit, no he fet separació per Mòduls Funcionals de l'aplicació. Simplement Pàgines i Components.
___

### Apunts

La Home és una simple vista on es mostra el llistat de Customers per poder navegar.

No he importat cap llibreria d'estils (Material, Boostrap, Chakra..).

He separat l'estil en una carpeta @Theme. Per component també es podria separar el fitxer d'estil.

No he fet dialog d'error per quan la API falla.

De BackEnd he fet el més senzill però a la vegada funcionant per capes.


Qualsevol dubte: octaf46@gmail.com

